Hardware Specs & Supply Chain
=============================

We would like to describe the underlaying hardware as detailed as possible to provide maximum transparency.

Based on this data it should be possible for customers to check if a hardware node can be trusted, where it was assebled, who installed it, etc.

`01-inital-hw-node-example.js` Shows a first example that illustrates the kind of data we would like to model.
`03-data-model-details.png` Tries to formalize this into a simple model
`09-ExampleNode.ttl` Models a subset of the above in turtle. This is only minimal as the current grammer does not really support our use-case yet.

Please see these files as examples/ideas and basic requirements. This is not a complete spec by any means.
