@startuml Gaia-X

package "Node" {

    class HardwareNode extends HwComponent {
        + totalDiskCapacity
        + totalMemoryCapacity
        + logEntries: LogEntry
        + hardwareComponents
    }

    class HwComponent {
        + brand
        + model
        + version
        + serialNumber
    }

    class MemoryComponent extends HwComponent {
        + capacity
    }

    class CpuComponent extends HwComponent {
        + cores
        + threads
        + minMhz
        + maxMhz
        + cache
        + tdp
    }

    class StorageComponent extends HwComponent {
        + type
        + interface
        + readSpeed
        + writeSpeed
        + readIops
        + writeIops
    }

    class MainboardComponent extends HwComponent {
        + biosType
        + managementEngine
        + remoteManagement
        + tpm
    }

    class NetworkComponent extends HwComponent {
        + dataRate
    }

    class PowerSupplyComponent extends HwComponent {
        + hotSwap
        + power
        + efficency
    }

    class CoolingComponent extends HwComponent {
        + type
        + powerUsage
    }

    class ProductionLog {
        + event
        + timestamp
        + signedOfBy
    }

    class Datacenter {
        + name
        + location
        + certification
    }

    HwComponent o-- "1"     Provider : manufacturer
    HwComponent o-- "1"     Provider : vendor
    HwComponent o-- "0..*"  ProductionLog : logs

    HardwareNode *-- "1..*" MemoryComponent
    HardwareNode *-- "1..*" CpuComponent
    HardwareNode *-- "1..*" StorageComponent
    HardwareNode *-- "1"    MainboardComponent
    HardwareNode *-- "1..*" NetworkComponent
    HardwareNode *-- "1..*" PowerSupplyComponent
    HardwareNode *-- "1..*" CoolingComponent
    HardwareNode o-- "1"    Provider

    Datacenter o-- "1..*"   HardwareNode
    Datacenter o-- "1"      Provider
}

package Provider {

    class Provider {
        + hasFullName
        + hasCommmercialRegister
        + hasRegisteredAddress
        + hasWebAddress
    }

}

package Service {

    class Service {
        + name
        + version
        + description
        + tags
        + features
        + variants
    }

    Service o-- "0..*" Service : uses
    Service o-- "0..*" HardwareNode : runs on
    Service o-- "1"    Provider
}

@enduml
