#!/usr/bin/env python3

import os
from functools import partial

import pyshacl
import rdflib
from colorama import Fore, Style
import yaml
import json
import sys


def load_file(filepath, format):
    g = rdflib.Graph()
    g.parse(filepath, format=format)
    # print('found {} triples'.format(len(g)))
    # for subject, predicate, obj in g:
    #     print(subject, predicate, obj)
    return g

def load_yaml(filepath):
    data = json.dumps(yaml.safe_load(open(filepath, 'r')))
    g = rdflib.Graph()
    g.parse(data=data, format='json-ld')
    return g


EXTS = {'.ttl': partial(load_file, format='turtle'),
        '.jsonld': partial(load_file, format='json-ld'),
        '.yamlld': load_yaml}


def check_self_descriptions(sd_root, shacl_graph=None):
    report = {}
    for dirname, _, filenames in os.walk(sd_root):
        for filename in filenames:
            for ext, loader in EXTS.items():  # loop over known extensions
                if not filename.lower().endswith(ext):  # if not a known extension
                    continue  # skip to next filename
                filepath = os.path.join(dirname, filename)
                print(filepath)
                try:
                    data_graph = loader(filepath)
                    if shacl_graph:
                        # Append controlled vocabulary to the data graph
                        certifications_graph = load_file('../implementation/ontology/ControlledVocabularies/Certifications.ttl', 'turtle')
                        for triple in certifications_graph:
                            data_graph.add(triple)

                        conforms, results_graph, results_text = pyshacl.validate(data_graph, shacl_graph=shacl_graph, inference='rdfs', abort_on_error=False,
                                                meta_shacl=False, advanced=False, js=False, debug=False)
                        if not conforms:
                            raise Exception(results_text)
                except rdflib.plugins.parsers.notation3.BadSyntax as ex:
                    print(Fore.RED + ex.message + Style.RESET_ALL)
                    report[filepath] = ex.message
                except Exception as ex:
                    print(Fore.RED + str(ex) + Style.RESET_ALL)
                    report[filepath] = str(ex)
    return report

def load_data_graph_with_controlled_vocabs(filepath, format):
    g = load_file(filepath, format)
    certification_vocab = load_file('../implementation/ontology/ControlledVocabularies/Certifications.ttl', 'turtle')
    for triple in certification_vocab :
        g.add(triple)
    return g


if __name__ == '__main__':
    report = {}
    report.update(check_self_descriptions('../implementation/ontology'))
    report.update(check_self_descriptions('../implementation/validation'))
    report.update(check_self_descriptions('../implementation/instances/provider', load_file('../implementation/validation/ParticipantShape.ttl', 'turtle')))
    report.update(check_self_descriptions('../implementation/instances/service', load_file('../implementation/validation/ServiceShape.ttl', 'turtle')))
    err_count = len(report)
    json.dump(report, sys.stderr)
    if err_count != 0:
        print(Style.BRIGHT + Fore.RED + 'found {} error(s)'.format(err_count))
    os.sys.exit(err_count)
